# chat_box

## Run ##
 move to project root folder


1. Create and activate a virtualenv (Python 3)
```bash
pipenv --python 3 shell
```
2. Install requirements
```bash
pipenv install
```
3. Init database
```bash
./manage.py migrate
```
4. Run tests
```bash
./manage.py test
```

5. Create admin user
```bash
./manage.py createsuperuser
```

6. Run development server
```bash
./manage.py runserver
```

To override default settings, create a local_settings.py file in the chat folder.

Message prefetch config (load last n messages):
```python
MESSAGES_TO_LOAD = 15
```